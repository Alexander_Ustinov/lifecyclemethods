package com.homework.lab4.livecyclemethods;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onPause() {
        super.onPause();
        showToast(LifeCycleMethods.OnCreate, Toast.LENGTH_SHORT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        showToast(LifeCycleMethods.OnDestroy, Toast.LENGTH_SHORT);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showToast(LifeCycleMethods.OnStart, Toast.LENGTH_SHORT);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        showToast(LifeCycleMethods.OnRestart, Toast.LENGTH_SHORT);
    }

    @Override
    protected void onStop() {
        super.onStop();
        showToast(LifeCycleMethods.OnStop, Toast.LENGTH_SHORT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showToast(LifeCycleMethods.OnResume, Toast.LENGTH_SHORT);
    }

    private void showToast(LifeCycleMethods lifeCycleMethodMessage, int delay) {
        Toast.makeText(
                getApplicationContext(),
                lifeCycleMethodMessage.toString(),
                delay
        ).show();
    }
}
