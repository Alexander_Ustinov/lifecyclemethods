package com.homework.lab4.livecyclemethods;

enum LifeCycleMethods {
    OnCreate,
    OnPause,
    OnDestroy,
    OnStart,
    OnStop,
    OnRestart,
    OnResume
}
