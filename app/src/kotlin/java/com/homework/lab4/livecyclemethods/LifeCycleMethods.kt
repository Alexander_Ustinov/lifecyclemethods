package com.homework.lab4.livecyclemethods

enum class LifeCycleMethods {
    OnCreate,
    OnPause,
    OnDestroy,
    OnStart,
    OnStop,
    OnRestart,
    OnResume
}