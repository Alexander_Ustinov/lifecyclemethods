package com.homework.lab4.livecyclemethods

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showToast(LifeCycleMethods.OnCreate, Toast.LENGTH_SHORT)
    }

    override fun onPause() {
        super.onPause()
        showToast(LifeCycleMethods.OnPause, Toast.LENGTH_SHORT)
    }

    override fun onDestroy() {
        super.onDestroy()
        showToast(LifeCycleMethods.OnDestroy, Toast.LENGTH_SHORT)
    }

    override fun onStart() {
        super.onStart()
        showToast(LifeCycleMethods.OnStart, Toast.LENGTH_SHORT)
    }

    override fun onRestart() {
        super.onRestart()
        showToast(LifeCycleMethods.OnRestart, Toast.LENGTH_SHORT)
    }

    override fun onStop() {
        super.onStop()
        showToast(LifeCycleMethods.OnStop, Toast.LENGTH_SHORT)
    }

    override fun onResume() {
        super.onResume()
        showToast(LifeCycleMethods.OnResume, Toast.LENGTH_SHORT)
    }

    private val showToast = {lifeCycleMethodMessage: LifeCycleMethods, delay: Int ->
        Toast.makeText(
            applicationContext,
            lifeCycleMethodMessage.toString().subSequence(0, lifeCycleMethodMessage.toString().length - 1),
            delay
        ).show()
    }
}
